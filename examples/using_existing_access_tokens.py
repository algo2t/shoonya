from time import sleep
import pandas as pd
from datetime import datetime, date
import json
from config import username, password, panno

from shoonya import Shoonya, TransactionType, ProductType, OrderType, InstrumentType

pd.set_option('display.width', 1500)
pd.set_option('display.max_columns', 75)
pd.set_option('display.max_rows', 1500)

ACCESS_FILE = f'params.json'
with open(ACCESS_FILE, "r") as access:
    credentials = json.load(access)
    access_token = credentials["access_token"]
    key = credentials['key']
    token = credentials["token_id"]
    username = credentials['user_id']
    usercode = credentials["usercode"]
    usertype = credentials["usertype"]
    panno = credentials['panno']


shn = Shoonya(username, access_token, panno, key,
              token, usercode, usertype, master_contracts_to_download=['NSE', 'NFO', 'MCX'])

bal = shn.get_limits()
print(bal)
df = pd.DataFrame(bal, index=None)
df['MTM'] = df['REALISED_PROFITS'] + df['MTM_COMBINED']
print(df[['MTM','REALISED_PROFITS', 'AMOUNT_UTILIZED',
      'CLEAR_BALANCE', 'AVAILABLE_BALANCE', 'MTM_COMBINED']])
# for _ in range(1, 10, 1):
#     print(shn.get_limits())
#     sleep(2)
scrip = shn.get_instrument_by_symbol('NSE', 'SBIN')
print(scrip)
# order1 = shn.place_order(transaction_type=TransactionType.Sell,
#                               instrument=scrip,
#                               InstrumentType=InstrumentType.Equity,
#                               quantity=1,
#                               order_type=OrderType.Limit,
#                               product_type=ProductType.Intraday,
#                               price=365.0,
#                               trigger_price=None,
#                               is_amo=True)
# print(order1)

scrip = shn.get_instrument_by_symbol('MCX', 'NATURALGAS MAY FUT')
print(scrip)
order1 = shn.place_order(transaction_type=TransactionType.Sell,
                              instrument=scrip,
                              InstrumentType=InstrumentType.Commodity_Future,
                              quantity=1,
                              order_type=OrderType.Limit,
                              product_type=ProductType.Intraday,
                              price=220,
                              trigger_price=None,
                              is_amo=True)
print(order1)


expiry = date(2021, 5, 20)
strike_price = 14600.0
nf_ce = shn.get_instrument_for_fno(symbol='NIFTY', expiry_date=expiry, is_fut=False,
                                                       strike=strike_price, is_CE=True)
print(nf_ce)
# 52105157469
# 
# print(shn.get_order_info(52105157469))
# print(shn.modify_order(52105157469, order_type=OrderType.Limit, quantity=1, price=366.0, trigger_price=365.0, is_amo=True))

# sleep(2)
# print(shn.get_order_info(52105157469))

# print(shn.cancel_order(52105157469))
sleep(2)
print(shn.get_orders())

scrip = shn.get_instrument_by_symbol('MCX', 'NATURALGAS MAY FUT')
print(scrip)
order1 = shn.place_order(transaction_type=TransactionType.Sell,
                              instrument=scrip,
                              InstrumentType=InstrumentType.Commodity_Future,
                              quantity=1,
                              order_type=OrderType.Limit,
                              product_type=ProductType.Intraday,
                              price=220,
                              trigger_price=None,
                              is_amo=True)
print(order1)
print('---------------------------------------------------')
print('---------------------------------------------------')

# expiry = date(2021, 5, 20)
# strike_price = 14600.0
# nf_ce = shn.get_instrument_for_fno(symbol='NIFTY', expiry_date=expiry, is_fut=False,
#                                                        strike=strike_price, is_CE=True)
# print(nf_ce)
# 52105157469
# 
print(shn.get_order_info(order1))
# print(shn.get_order_info(52105217119))
print(shn.modify_order(order1, order_type=OrderType.Limit, quantity=1, price=221.0, trigger_price=221.0, is_amo=True))

sleep(5)
print(shn.get_order_info(order1))

# print(shn.cancel_order(52105157469))
# print(shn.cancel_order(order1))
print('---------------------------------------------------')
print('---------------------------------------------------')
sleep(2)

data = shn.get_orders()
df = pd.DataFrame(data, index=None)
print(df[['SYMBOL', 'INSTRUMENT', 'ORDER_NUMBER', 'EXPIRY_DATE', 'PRODUCT', 'STATUS', 'QUANTITY', 'PRICE', 'TRG_PRICE', 'ORDER_DATE_TIME']])
sleep(2)
for order_number in df['ORDER_NUMBER'].tolist():
    print(shn.cancel_order(order_number))


data = shn.get_orders()
df = pd.DataFrame(data, index=None)
print(df[['SYMBOL', 'INSTRUMENT', 'ORDER_NUMBER', 'EXPIRY_DATE', 'PRODUCT', 'STATUS', 'QUANTITY', 'PRICE', 'TRG_PRICE', 'ORDER_DATE_TIME']])

