from time import sleep
import pandas as pd
from datetime import datetime, date
import json
from config import username, password, panno

from shoonya import Shoonya

pd.set_option('display.width', 1500)
pd.set_option('display.max_columns', 75)
pd.set_option('display.max_rows', 1500)

start = datetime.now()

print(start)

access_params = Shoonya.login_and_get_authorizations(username=username, password=password, panno=panno)

print(access_params)
with open('params.json', 'w') as wrl:
    json.dump(access_params, wrl)

sleep(1)
ACCESS_FILE = f'params.json'
with open(ACCESS_FILE, "r") as access:
    credentials = json.load(access)
    access_token = credentials["access_token"]
    key = credentials['key']
    token = credentials["token_id"]
    username = credentials['user_id']
    usercode = credentials["usercode"]
    usertype = credentials["usertype"]
    panno = credentials['panno']


shn = Shoonya(username, access_token, panno, key,
              token, usercode, usertype, master_contracts_to_download=['NSE'])

bal = shn.get_limits()
print(bal)
quit()

